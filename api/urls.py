from django.urls import path, include
from rest_framework import routers
from api.viewsets import UserViewSet, GroupViewSet, CategoryViewSet, ProductViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet, basename='User')
router.register(r'group', GroupViewSet, basename='Group')
router.register(r'category', CategoryViewSet, basename='Category')
router.register(r'product', ProductViewSet, basename='Product')


urlpatterns = [
    path('', include(router.urls)),
]
