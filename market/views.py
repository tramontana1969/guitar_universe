from django.shortcuts import render, get_object_or_404
from .models import Product, Category
from cart.forms import CartAddProductForm


def home(request):
    products = Product.objects.all()
    context = {
        'products': products,
    }
    return render(request, 'market/home.html', context)


def products_in_category(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    products = Product.objects.filter(category=category)
    context = {
        'products': products,
    }
    return render(request, 'market/home.html', context)


def item_detail(request, product_slug, id):
    product = get_object_or_404(Product, slug=product_slug, id=id)
    cart_product_form = CartAddProductForm()
    context = {
        'product': product,
        'cart_product_form': cart_product_form,
    }
    return render(request, 'market/item.html', context)
