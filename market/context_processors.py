from market.models import Category
from cart.cart import Cart


def category(request):
    return {'category': Category.objects.all()}


def cart(request):
    return {'cart': Cart(request)}
