from django.urls import path
from market.views import home, products_in_category, item_detail

urlpatterns = [
    path('', home, name='home'),
    path('<slug:category_slug>/', products_in_category, name='category'),
    path('product/<slug:product_slug>/<int:id>/', item_detail, name='item'),
]
