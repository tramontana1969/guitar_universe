from django.db import models
from django.utils.text import slugify
from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
    parent = TreeForeignKey(
        'self',
        blank=True,
        null=True,
        related_name='children',
        on_delete=models.CASCADE,
    )
    title = models.CharField(
        max_length=50,
        verbose_name='title',
    )
    slug = models.SlugField(
        verbose_name='slug',
        unique=True,
    )

    class MPTTMeta:
        order_insertion_by = ['title']

    def __str__(self):
        return f'{self.title}'


class Product(models.Model):
    title = models.CharField(
        max_length=250,
        verbose_name='Product',
    )
    text = models.TextField(
        verbose_name='Text',
    )
    price = models.DecimalField(
        decimal_places=2,
        verbose_name='Price',
        max_digits=10,
    )
    category = models.ForeignKey(
        'Category',
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    img = models.ImageField(
        verbose_name='Picture'
    )
    slug = models.SlugField(
        verbose_name='Slug',
        unique=True,
    )

    def __str__(self):
        return f'{self.title}, {self.price}'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Product, self).save(*args, **kwargs)
